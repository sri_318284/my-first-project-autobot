package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;


public class CreateLeadPage extends Annotations{ 

	

	/*@FindBy(how=How.ID, using="userid")  WebElement eleUserName;
	@FindBy(how=How.ID, using="password")  WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;*/

	public CreateLeadPage enterCompName()
	{
		
		WebElement enterCompName = locateElement("id","createLeadForm_companyName");
		clearAndType(enterCompName, "compname");
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterFirstName()
	{
		
		WebElement enterFirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(enterFirstName, "firstname");
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterLastName()
	{
		
		WebElement enterLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(enterLastName, "lastname");
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}
	
	public ViewLeadPage clickOnCreateLead()
	{
		WebElement clickOnCreateLead = locateElement("xpath","//input[@value='Create Lead']");
		click(clickOnCreateLead);
//		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return new ViewLeadPage();
	}
}







