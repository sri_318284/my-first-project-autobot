package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC0002_CreateLead";
		testcaseDec = "Login into leaftaps";
		author = "srikanth";
		category = "smoke";
		excelFileName = "TC0002";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickONCRMSFALink()
		.clickOnLeadsLink()
		.clickOnCreateLeadLink()
		.enterCompName()
		.enterFirstName()
		.enterLastName()
		.clickOnCreateLead()
		.verifyUpdatedName();
		
	}
	
}






