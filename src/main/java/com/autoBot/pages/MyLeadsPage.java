package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;


public class MyLeadsPage extends Annotations{ 

	

	/*@FindBy(how=How.ID, using="userid")  WebElement eleUserName;
	@FindBy(how=How.ID, using="password")  WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;*/

	
	public CreateLeadPage clickOnCreateLeadLink()
	
	
	{
		
		WebElement eleClickOnCreateLead = locateElement("link","Create Lead");
		click(eleClickOnCreateLead);
//		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
	
	 public FindLeadsPage clickOnFindLeadsLink() throws InterruptedException
	 {
		 
		 WebElement eleClickOnFindLeads = locateElement("link","Find Leads");
			click(eleClickOnFindLeads);
//		 driver.findElementByLinkText("Find Leads").click();
		 Thread.sleep(5000);
		 return new FindLeadsPage();
	 }
	

}







