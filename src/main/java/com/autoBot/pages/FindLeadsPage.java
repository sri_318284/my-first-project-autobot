package com.autoBot.pages;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;


public class FindLeadsPage extends Annotations{ 

	

	/*@FindBy(how=How.ID, using="userid")  WebElement eleUserName;
	@FindBy(how=How.ID, using="password")  WebElement elePassWord;
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;*/

	
	public FindLeadsPage enterFirstName(String data)
	{
		
		WebElement eleEnterFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		
	
//		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(data);
		return this;
		
	}
		
		
	
	
	public FindLeadsPage clickOnFindLeadsButton() throws InterruptedException
	{
		
		WebElement eleClickOnFindLeadsButton = locateElement("xpath","//button[text()='Find Leads']");
		click(eleClickOnFindLeadsButton);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		return this;
	

}
	
	
}







